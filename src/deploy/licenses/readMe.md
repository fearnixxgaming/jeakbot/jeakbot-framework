# Distribution licenses
The JeakBot framework would not be possible without the use of other freely available libraries.  
We list the main libraries and their license in this directory.  
Transitive dependencies are listed in the license file of the library that requires them.